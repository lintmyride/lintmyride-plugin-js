import { AvailablePresets, AvailableRules } from "lintmyride";
import * as duplicateLockfile from "./rules/duplicate-lockfile";
import * as lockfile from "./rules/lockfile";
import * as nvm from "./rules/nvm";
import * as sideEffects from "./rules/side-effects";
import * as recommendedPreset from "./presets/recommended";

export const presets: AvailablePresets = [
  recommendedPreset,
];

export const rules: AvailableRules = [ 
  duplicateLockfile,
  lockfile,
  nvm,
  sideEffects,
];
