import { describe, it, expect } from "@jest/globals";
import { DirNode, FileNode, getEnterVisitor, VisitorContext } from "lintmyride";
import { createVisitors } from "./lockfile";

describe("js/lockfile", () => {
  const mockContext: VisitorContext = {};

  it("rejects npm packages without a lock file", () => {
    const dirNode: DirNode = {
      type: "Dir",
      path: "",
      name: "arbitrary-dir",
      children: ["package.json"]
    };

    const visitors = createVisitors(mockContext);
    const dirVisitor = getEnterVisitor(visitors.Dir);

    expect(dirVisitor(dirNode)).toEqual({
      output: "Node projects should include a lock file.",
    });
  });

  it("is OK with Yarn lock files.", () => {
    const dirNode: DirNode = {
      type: "Dir",
      path: "",
      name: "arbitrary-dir",
      children: ["package.json", "yarn.lock"]
    };

    const visitors = createVisitors(mockContext);
    const dirVisitor = getEnterVisitor(visitors.Dir);

    expect(dirVisitor(dirNode)).toBeUndefined();
  });

  it("is OK with npm lock files.", () => {
    const dirNode: DirNode = {
      type: "Dir",
      path: "",
      name: "arbitrary-dir",
      children: ["package.json", "package-lock.json"]
    };

    const visitors = createVisitors(mockContext);
    const dirVisitor = getEnterVisitor(visitors.Dir);

    expect(dirVisitor(dirNode)).toBeUndefined();
  });

  it("does not act on sub-directories (which may be workspaces)", () => {
    const dirNode: DirNode = {
      type: "Dir",
      path: "not-the-root/",
      name: "not-the-root",
      children: ["package.json"]
    };

    const visitors = createVisitors(mockContext);
    const dirVisitor = getEnterVisitor(visitors.Dir);

    expect(dirVisitor(dirNode)).toBeUndefined();
  });

  it("does not act on files", () => {
    const fileNode: FileNode = {
      type: "File",
      path: "some-file.ts",
      name: "some-file.ts",
      contents: "Arbitrary contents",
    };

    const visitors = createVisitors(mockContext);
    const dirVisitor = getEnterVisitor(visitors.Dir);

    expect(dirVisitor(fileNode as any)).toBeUndefined();
  });
});
