import { CreateVisitors, RuleId, FileNode  } from "lintmyride";

export const ruleId: RuleId = "js/gitignore";

export const createVisitors: CreateVisitors<FileNode> = (context) => {
  let topLevelPackageJson: boolean = false;

  return {
    File: {
      enter: (node) => {
        if (node.name === "package.json" && node.path === "") {
          topLevelPackageJson = true;
        }
      },
      exit: (node) => {
        if (!topLevelPackageJson || node.name !== ".gitignore" || node.path !== "") {
          return;
        }
        // A line that refers to node_modules, optionally:
        // - prefixed by ./ or .\
        // - suffixed by /* or \*
        //   but not other characters that would indicate files or folders inside node_modules:
        const nodeModulesLineRegex = /^(.\/|.\\)?node_modules(\/\*|\\\*)?$/gm;
        if(!nodeModulesLineRegex.test(node.contents)) {
          return {
            output: "node_modules should not be committed, and hence should be listed in .gitignore."
          };
        }
      },
    },
  };
};
