import { describe, it, expect } from "@jest/globals";
import { DirNode, FileNode, getEnterVisitor, VisitorContext } from "lintmyride";
import { createVisitors } from "./nvm";

describe("js/nvm", () => {
  const mockContext: VisitorContext = {};

  it("rejects npm packages without an .nvmrc", () => {
    const dirNode: DirNode = {
      type: "Dir",
      path: "",
      name: "arbitrary-dir",
      children: ["package.json"]
    };

    const visitors = createVisitors(mockContext);
    const dirVisitor = getEnterVisitor(visitors.Dir);

    expect(dirVisitor(dirNode)).toEqual({
      output: "Node projects should include an .nvmrc file to indicate the expected Node/npm versions.",
    });
  });

  it("does not act on sub-directories (in which `nvm use` will not be run)", () => {
    const dirNode: DirNode = {
      type: "Dir",
      path: "not-the-root/",
      name: "not-the-root",
      children: ["package.json"]
    };

    const visitors = createVisitors(mockContext);
    const dirVisitor = getEnterVisitor(visitors.Dir);

    expect(dirVisitor(dirNode)).toBeUndefined();
  });

  it("does not act on files", () => {
    const fileNode: FileNode = {
      type: "File",
      path: "some-file.ts",
      name: "some-file.ts",
      contents: "Arbitrary contents",
    };

    const visitors = createVisitors(mockContext);
    const dirVisitor = getEnterVisitor(visitors.Dir);

    expect(dirVisitor(fileNode as any)).toBeUndefined();
  });
});
