import { describe, it, expect } from "@jest/globals";
import { FileNode, getEnterVisitor, VisitorContext, getExitVisitor } from "lintmyride";
import { createVisitors } from "./gitignore";

describe("js/gitignore", () => {
  const mockContext: VisitorContext = {};

  it("rejects npm packages that do not add node_modules to their gitignore", () => {
    const packageJsonNode: FileNode = {
      type: "File",
      path: "",
      name: "package.json",
      contents: '{ "name": "arbitrary-package" }',
    };
    const gitignoreNode: FileNode = {
      type: "File",
      path: "",
      name: ".gitignore",
      contents: "",
    };

    const visitors = createVisitors(mockContext);
    const fileEnterVisitor = getEnterVisitor(visitors.File);
    const fileExitVisitor = getExitVisitor(visitors.File);

    fileEnterVisitor(packageJsonNode);

    expect(fileExitVisitor(gitignoreNode))
      .toEqual({
        output: "node_modules should not be committed, and hence should be listed in .gitignore.",
      });
  });

  it("allows a plain node_modules entry in .gitignore", () => {
    const packageJsonNode: FileNode = {
      type: "File",
      path: "",
      name: "package.json",
      contents: '{ "name": "arbitrary-package" }',
    };
    const gitignoreNode: FileNode = {
      type: "File",
      path: "",
      name: ".gitignore",
      contents: "node_modules",
    };

    const visitors = createVisitors(mockContext);
    const fileEnterVisitor = getEnterVisitor(visitors.File);
    const fileExitVisitor = getExitVisitor(visitors.File);

    fileEnterVisitor(packageJsonNode);

    expect(fileExitVisitor(gitignoreNode)).toBeUndefined();
  });

  it("allows a relative node_modules entry in .gitignore", () => {
    const packageJsonNode: FileNode = {
      type: "File",
      path: "",
      name: "package.json",
      contents: '{ "name": "arbitrary-package" }',
    };
    const gitignoreNode: FileNode = {
      type: "File",
      path: "",
      name: ".gitignore",
      contents: "./node_modules",
    };

    const visitors = createVisitors(mockContext);
    const fileEnterVisitor = getEnterVisitor(visitors.File);
    const fileExitVisitor = getExitVisitor(visitors.File);

    fileEnterVisitor(packageJsonNode);

    expect(fileExitVisitor(gitignoreNode)).toBeUndefined();
  });

  it("allows a globbing node_modules entry in .gitignore", () => {
    const packageJsonNode: FileNode = {
      type: "File",
      path: "",
      name: "package.json",
      contents: '{ "name": "arbitrary-package" }',
    };
    const gitignoreNode: FileNode = {
      type: "File",
      path: "",
      name: ".gitignore",
      contents: "node_modules/*",
    };

    const visitors = createVisitors(mockContext);
    const fileEnterVisitor = getEnterVisitor(visitors.File);
    const fileExitVisitor = getExitVisitor(visitors.File);

    fileEnterVisitor(packageJsonNode);

    expect(fileExitVisitor(gitignoreNode)).toBeUndefined();
  });

  it("does not require ignoring node_modules if you do not have a top-level package.json", () => {
    const fileNode: FileNode = {
      type: "File",
      path: "",
      name: ".gitignore",
      contents: "",
    };

    const visitors = createVisitors(mockContext);
    const fileVisitor = getExitVisitor(visitors.File);

    expect(fileVisitor(fileNode)).toBeUndefined();
  });

  // Longer term, this rule _will_ check for matching rules in your .gitignore,
  // but since this requires checking .gitignore in subdirectories as well,
  // in addition to also having to validate that the entry in .gitignore matches that subdirectory,
  // that was more complicated to implement and thus hasn't been implemented yet.
  it("does not require ignoring node_modules if your package.json is in a subdirectory", () => {
    const packageJsonNode: FileNode = {
      type: "File",
      path: "subdir/",
      name: "package.json",
      contents: '{ "name": "arbitrary-package" }',
    };
    const gitignoreNode: FileNode = {
      type: "File",
      path: "",
      name: ".gitignore",
      contents: "",
    };

    const visitors = createVisitors(mockContext);
    const fileEnterVisitor = getEnterVisitor(visitors.File);
    const fileExitVisitor = getExitVisitor(visitors.File);

    fileEnterVisitor(packageJsonNode);

    expect(fileExitVisitor(gitignoreNode)).toBeUndefined();
  });
});
