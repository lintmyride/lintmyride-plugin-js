import { DirNode, isDirNode, isRootDir, CreateVisitors  } from "lintmyride";

export const ruleId = "js/lockfile";

export const createVisitors: CreateVisitors<DirNode> = (context) => {
  return {
    Dir: (node) => {
      if (!isDirNode(node)) {
        return;
      }

      if (
        isRootDir(node) &&
        node.children.includes("package-lock.json") &&
        node.children.includes("yarn.lock")
      ) {
        return {
          output: "To prevent outdated lockfiles, Node projects should not include both Yarn and npm lockfiles.",
        };
      }
    },
  };
};
