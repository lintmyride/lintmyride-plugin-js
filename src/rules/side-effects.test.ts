import { describe, it, expect } from "@jest/globals";
import { DirNode, FileNode, getEnterVisitor, VisitorContext } from "lintmyride";
import { createVisitors } from "./side-effects";

describe("js/lockfile", () => {
  const mockContext: VisitorContext = {};

  it("rejects npm packages that do not indicate whether they are side-effect-free", () => {
    const fileNode: FileNode = {
      type: "File",
      path: "",
      name: "package.json",
      contents: '{ "name": "arbitrary-package" }',
    };

    const visitors = createVisitors(mockContext);
    const fileVisitor = getEnterVisitor(visitors.File);

    expect(fileVisitor(fileNode)).toEqual({
      output: "Node projects should indicate whether they are side-effect-free.",
      url: "https://webpack.js.org/guides/tree-shaking/#mark-the-file-as-side-effect-free",
    });
  });

  it("is OK with packages that contain side-effects.", () => {
    const fileNode: FileNode = {
      type: "File",
      path: "",
      name: "package.json",
      contents: '{ "name": "arbitrary-package", "sideEffects": true }',
    };

    const visitors = createVisitors(mockContext);
    const fileVisitor = getEnterVisitor(visitors.File);

    expect(fileVisitor(fileNode)).toBeUndefined();
  });

  it("is OK with packages that are side-effect-free.", () => {
    const fileNode: FileNode = {
      type: "File",
      path: "",
      name: "package.json",
      contents: '{ "name": "arbitrary-package", "sideEffects": false }',
    };

    const visitors = createVisitors(mockContext);
    const fileVisitor = getEnterVisitor(visitors.File);

    expect(fileVisitor(fileNode)).toBeUndefined();
  });

  it("does not act on files that are not package.json", () => {
    const fileNode: FileNode = {
      type: "File",
      path: "",
      name: "not-package.json",
      contents: '{ "name": "arbitrary-package", "sideEffects": true }',
    };

    const visitors = createVisitors(mockContext);
    const fileVisitor = getEnterVisitor(visitors.File);

    expect(fileVisitor(fileNode)).toBeUndefined();
  });

  it("does not act on directories", () => {
    const dirNode: DirNode = {
      type: "Dir",
      path: "",
      name: "arbitrary-dir",
      children: [],
    };

    const visitors = createVisitors(mockContext);
    const fileVisitor = getEnterVisitor(visitors.File);

    expect(fileVisitor(dirNode as any)).toBeUndefined();
  });
});
