import { describe, it, expect } from "@jest/globals";
import { DirNode, FileNode, getEnterVisitor, VisitorContext } from "lintmyride";
import { createVisitors } from "./duplicate-lockfile";

describe("js/duplicate-lockfile", () => {
  const mockContext: VisitorContext = {};

  it("rejects npm packages with both an npm and Yarn lock file", () => {
    const dirNode: DirNode = {
      type: "Dir",
      path: "",
      name: "arbitrary-dir",
      children: ["package.json", "package-lock.json", "yarn.lock"]
    };

    const visitors = createVisitors(mockContext);
    const dirVisitor = getEnterVisitor(visitors.Dir);

    expect(dirVisitor(dirNode)).toEqual({
      output: "To prevent outdated lockfiles, Node projects should not include both Yarn and npm lockfiles.",
    });
  });

  it("is OK with just Yarn lock files.", () => {
    const dirNode: DirNode = {
      type: "Dir",
      path: "",
      name: "arbitrary-dir",
      children: ["package.json", "yarn.lock"]
    };

    const visitors = createVisitors(mockContext);
    const dirVisitor = getEnterVisitor(visitors.Dir);

    expect(dirVisitor(dirNode)).toBeUndefined();
  });

  it("is OK with just npm lock files.", () => {
    const dirNode: DirNode = {
      type: "Dir",
      path: "",
      name: "arbitrary-dir",
      children: ["package.json", "package-lock.json"]
    };

    const visitors = createVisitors(mockContext);
    const dirVisitor = getEnterVisitor(visitors.Dir);

    expect(dirVisitor(dirNode)).toBeUndefined();
  });

  it("does not act on files", () => {
    const fileNode: FileNode = {
      type: "File",
      path: "some-file.ts",
      name: "some-file.ts",
      contents: "Arbitrary contents",
    };

    const visitors = createVisitors(mockContext);
    const dirVisitor = getEnterVisitor(visitors.Dir);

    expect(dirVisitor(fileNode as any)).toBeUndefined();
  });
});
