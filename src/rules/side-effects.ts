import { FileNode, isFileNode, CreateVisitors } from "lintmyride";

export const ruleId = "js/lockfile";

export const createVisitors: CreateVisitors<FileNode> = (context) => {
  return {
    File: (node) => {
      if (!isFileNode(node) || node.name !== "package.json") {
        return;
      }

      const pkg = JSON.parse(node.contents);

      if (typeof pkg.sideEffects !== "boolean") {
        return {
          output: "Node projects should indicate whether they are side-effect-free.",
          url: "https://webpack.js.org/guides/tree-shaking/#mark-the-file-as-side-effect-free",
        };
      }
    },
  };
};
