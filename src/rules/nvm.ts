import { DirNode, isDirNode, isRootDir, CreateVisitors  } from "lintmyride";

export const ruleId = "js/nvm";

export const createVisitors: CreateVisitors<DirNode> = (context) => {
  return {
    Dir: (node) => {
      if(!isDirNode(node)) {
        return;
      }
      if (
        isRootDir(node) &&
        node.children.includes("package.json") &&
        !node.children.includes(".nvmrc")
      ) {
        return {
          output: "Node projects should include an .nvmrc file to indicate the expected Node/npm versions.",
        };
      }
    },
  };
};
