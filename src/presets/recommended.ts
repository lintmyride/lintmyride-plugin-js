import { RulesConfig, PresetOptions, PresetId } from "lintmyride";

export const presetId: PresetId = "js/recommended";

const rules: RulesConfig = {
  "js/duplicate-lockfile": "error",
  "js/gitignore": "error",
  "js/lockfile": "warn",
  "js/nvm": "warn",
};

export const preset: PresetOptions = {
  rules: rules,
};
